program replicate
  implicit none

  integer, parameter :: dp = kind(1.0d0)
  integer :: i, j, k, no_atoms, rep_no, rep_dim, num_args
  real(dp), parameter :: pi = 3.141592653589793238462643383279502884197_dp
  real(dp) :: x, y, z, cell_length
  real(dp), dimension(3) :: cell_dims, min_pos, max_pos, centre_pos, sfac
  real(dp), allocatable, dimension(:,:) :: positions, new_positions
  real(dp), allocatable, dimension(:) :: pol
  character(len=256) :: comment, units, dummy
  character(len=2), allocatable, dimension(:) :: atoms

  logical :: periodic, centre, polarized

  num_args = iargc()
  if (num_args/=6) then
     stop 'Error, usage: cell length, num reps, dim to rep in, periodic? centre? polarization?. cell_in.xyz -> cell_out.xyz'
  end if
  call getarg(1,dummy)
  read (dummy,*) cell_length
  call getarg(2,dummy)
  read (dummy,*) rep_no
  call getarg(3,dummy)
  read (dummy,*) rep_dim
  call getarg(4,dummy)
  read (dummy,*) periodic
  call getarg(5,dummy)
  read (dummy,*) centre
  call getarg(6,dummy)
  read (dummy,*) polarized

  !print*,'Enter length of cell'
  !read*,cell_length
  !print*,'Enter number of times to replicate'
  !read*,rep_no
  !print*,'Enter dimension to replicate in'
  !read*,rep_dim

  open(10,file='cell_in.xyz')
  open(11,file='cell_out.xyz')

  read(10,*) no_atoms,units

  allocate(positions(1:no_atoms,1:3))
  !allocate(new_positions(1:no_atoms,1:3))
  allocate(atoms(1:no_atoms))
  if (polarized) allocate(pol(1:no_atoms))

  if (periodic) then
     read(10,*) comment,cell_dims(:)
  else
     read(10,*) comment
  end if

  do i=1,no_atoms
    if (polarized) then
       read(10,*) atoms(i),positions(i,:), pol(i)
    else
       read(10,*) atoms(i),positions(i,:)
    end if
  end do
  
  if (centre) then
    max_pos(:) = positions(1,:)
    min_pos(:) = positions(1,:)

    do i=2,no_atoms
      do j=1,3
        if (positions(i,j) > max_pos(j)) then
          max_pos(j) = positions(i,j)
        end if
        if (positions(i,j) < min_pos(j)) then
          min_pos(j) = positions(i,j)
        end if
      end do
    end do

    centre_pos(:) = 0.0_dp

    do j=1,3
      centre_pos(j) = 0.5_dp * (max_pos(j) + min_pos(j))
      !print*,j,min_pos(j),max_pos(j),centre_pos(j)
    end do
   
    !if periodic centre in box not along 0-axis
    if (periodic) then
       do j=1,3
          centre_pos(j) = centre_pos(j) - 0.5d0*cell_dims(j)
       end do
    end if

    do j=1,3
     !if (j==rep_dim) cycle
      positions(:,j) = positions(:,j) - centre_pos(j)
    end do
  end if

  sfac=1.0d0
  !override input length
  if (periodic .and. trim(units)/='reduced') then
     cell_length=cell_dims(rep_dim)
     cell_dims(rep_dim)=cell_length*rep_no
  else if (periodic) then
     cell_length=1.0d0
     cell_dims(rep_dim)=cell_dims(rep_dim)*rep_no
     sfac(rep_dim)=1.0d0/real(rep_no,dp)
  end if


  write(11,*) no_atoms*rep_no,trim(units)
  if (periodic) then
     write(11,'(A,3(2x,F18.6))') trim(comment),cell_dims(:)
  else
     write(11,*) trim(comment)
  end if
  do j=0,rep_no-1
    do i=1,no_atoms
      if (rep_dim==1) then
        if (polarized) then
          write(11,'(A,4(2x,F18.6))') atoms(i), (positions(i,1)+real(j,dp)*cell_length)*sfac(1), &
               & positions(i,2), positions(i,3), pol(i)
        else
          write(11,'(A,3(2x,F18.6))') atoms(i), (positions(i,1)+real(j,dp)*cell_length)*sfac(1), &
               & positions(i,2), positions(i,3)
        end if
      else if (rep_dim==2) then
        if (polarized) then
          write(11,'(A,4(2x,F18.6))') atoms(i), positions(i,1), &
               & (positions(i,2)+real(j,dp)*cell_length)*sfac(2), positions(i,3), pol(i)
        else
          write(11,'(A,3(2x,F18.6))') atoms(i), positions(i,1), &
               & (positions(i,2)+real(j,dp)*cell_length)*sfac(2), positions(i,3)
        end if
      else if (rep_dim==3) then
        if (polarized) then
          write(11,'(A,4(2x,F18.6))') atoms(i), positions(i,1), &
               & positions(i,2), (positions(i,3)+real(j,dp)*cell_length)*sfac(3), pol(i)
        else
          write(11,'(A,3(2x,F18.6))') atoms(i), positions(i,1), &
               & positions(i,2), (positions(i,3)+real(j,dp)*cell_length)*sfac(3)
        end if
      else
        print*,'Error error abort warning'
      end if
    end do
  end do  
  
  deallocate(atoms)
  !deallocate(new_positions)
  deallocate(positions)
  if (polarized) deallocate(pol)

  close(11)
  close(10) 
 
end program replicate










